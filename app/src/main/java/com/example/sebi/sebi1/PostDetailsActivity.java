package com.example.sebi.sebi1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.User;

public class PostDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.posts_details);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Posts postareTransmisaa = bundle.getParcelable("postare transmisaa");


        TextView postUserIDTextView = findViewById(R.id.postUserID_textview);
        TextView postIDTextView = findViewById(R.id.postid_textview);
        TextView postTitleTextView = findViewById(R.id.postTitle_textview);
        TextView postBodyTextView = findViewById(R.id.body_textview);

        postUserIDTextView.setText(postareTransmisaa.postUserId);

        postIDTextView.setText(postareTransmisaa.postid);
        postTitleTextView.setText(postareTransmisaa.postTitle);
        postBodyTextView.setText(postareTransmisaa.postBody);

    }
}
