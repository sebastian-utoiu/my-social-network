package com.example.sebi.sebi1.networkobjects;

/*{
        "userId": 1,
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        },

 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Posts implements Parcelable {

    @SerializedName(value = "postUserId", alternate = {"userId"})
    public String postUserId;

    @SerializedName(value = "postId", alternate = {"id"})
    public String postid;

    @SerializedName(value = "postTitle", alternate = {"title"})
    public String postTitle;

    @SerializedName(value = "postBody", alternate = {"body"})
    public String postBody;


    public static final Creator<Posts> CREATOR = new Creator<Posts>() {
        @Override
        public Posts createFromParcel(Parcel in) { return new Posts(in);
        }

        @Override
        public Posts[] newArray(int size) { return new Posts[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {

        destination.writeString(postUserId);
        destination.writeString(postid);
        destination.writeString(postTitle);
        destination.writeString(postBody);

    }


    protected Posts(Parcel in) {
        postUserId = in.readString();
        postid = in.readString();
        postTitle = in.readString();
        postBody= in.readString();


    }


}
