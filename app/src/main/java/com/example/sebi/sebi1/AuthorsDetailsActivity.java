package com.example.sebi.sebi1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Author;

public class AuthorsDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authors_details);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Author authorTransmis = bundle.getParcelable("authors transmis");


        TextView FirstNameTextView = findViewById(R.id.FirstName_textview);
        TextView IDTextView = findViewById(R.id.ID_textview);
        TextView IDBooksTextView = findViewById(R.id.IDBook_textview);
        TextView LastNameTextView = findViewById(R.id.LastName_textview);

        FirstNameTextView.setText(authorTransmis.FirstName);
        IDTextView.setText(authorTransmis.ID);
        IDBooksTextView.setText(authorTransmis.IDBook);
        LastNameTextView.setText(authorTransmis.LastName);

    }
}
