package com.example.sebi.sebi1.networkobjects;

import android.os.Parcel;
import android.os.Parcelable;

public class Author implements Parcelable {

    public String FirstName;
    public String ID;
    public String IDBook;
    public String LastName;

    protected Author(Parcel in) {
        FirstName = in.readString();
        ID = in.readString();
        IDBook = in.readString();
        LastName = in.readString();
    }

    public static final Creator<Author> CREATOR = new Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel in) {
            return new Author(in);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(FirstName);
        destination.writeString(ID);
        destination.writeString(IDBook);
        destination.writeString(LastName);
    }
}
