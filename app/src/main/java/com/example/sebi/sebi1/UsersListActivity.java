package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;
import com.example.sebi.sebi1.networkobjects.Todos;
import com.example.sebi.sebi1.networkobjects.Users;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class UsersListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_list_activity);

        recyclerView = findViewById(R.id.users_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Users[] totiUtilizatorii = ServiceConnectionManager.getInstance().getTotiUtilizatoriiFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaUtilizatori(totiUtilizatorii);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();


    }

    class  UsersViewHolder extends  RecyclerView.ViewHolder{

        TextView UsersID;
        TextView UsersPassword;
        TextView UserName;




        public UsersViewHolder(View itemView) {
            super(itemView);

            UsersID= itemView.findViewById(R.id.Users_ID);
            UsersPassword = itemView.findViewById(R.id.Users_Password);
            UserName = itemView.findViewById(R.id.UserName);


        }
    }

    public void afiseazaUtilizatori(final Users[] users)
    {


        final ArrayList listaUtilizatori= new ArrayList<>(Arrays.asList(users));

        final Activity activity = this;



        RecyclerView.Adapter usersAdapter = new RecyclerViewArrayListAdapter<Users, UsersListActivity.UsersViewHolder>(listaUtilizatori) {

            @NonNull
            @Override
            public UsersListActivity.UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.users_list_item, parent, false);
                UsersListActivity.UsersViewHolder ss = new UsersListActivity.UsersViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull UsersListActivity.UsersViewHolder myHolder, int position) {


                final Users myUtilizator = getData().get(position);

                myHolder.UsersID.setText(myUtilizator.UsersID);
                myHolder.UsersPassword.setText(myUtilizator.UsersPassword);
                myHolder.UserName.setText(myUtilizator.UserName);





                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click pe" + myUtilizator.UsersID,activity);

                        Intent myIntent = new Intent(activity, UsersDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("Utilizator transmis", myUtilizator);
                        myIntent.putExtras(b);
                        UsersListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(usersAdapter);

    }
}
