package com.example.sebi.sebi1.networkobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Users implements Parcelable {

    @SerializedName(value = "UsersID", alternate = {"ID"})
    public String UsersID;

    @SerializedName(value = "UsersPassword", alternate = {"Password"})
    public String UsersPassword;

    public String UserName;


    protected Users(Parcel in) {
        UsersID = in.readString();
        UsersPassword = in.readString();
        UserName = in.readString();
    }

    public static final Creator<Users> CREATOR = new Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel in) {
            return new Users(in);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(UsersID);
        parcel.writeString(UsersPassword);
        parcel.writeString(UserName);
    }
}
