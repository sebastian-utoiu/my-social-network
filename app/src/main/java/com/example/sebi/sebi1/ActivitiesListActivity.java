package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Activities;
import com.example.sebi.sebi1.networkobjects.Users;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class ActivitiesListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activities_list_activity);

        recyclerView = findViewById(R.id.activities_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Activities[] allActivities = ServiceConnectionManager.getInstance().getAllActivitiesFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaActivities(allActivities);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();


    }


    class  ActivitiesViewHolder extends  RecyclerView.ViewHolder{

        TextView Completed;
        TextView dueDate;
        TextView ActivitiesID;
        TextView ActivitiesTitle;




        public ActivitiesViewHolder(View itemView) {
            super(itemView);

            Completed= itemView.findViewById(R.id.Completed_activities);
            dueDate = itemView.findViewById(R.id.dueDate_activities);
            ActivitiesID = itemView.findViewById(R.id.ID_activities);
            ActivitiesTitle = itemView.findViewById(R.id.Title_activities);


        }
    }

    public void afiseazaActivities(final Activities[] activities)
    {


        final ArrayList listaActivities= new ArrayList<>(Arrays.asList(activities));

        final Activity activity = this;



        RecyclerView.Adapter activitiesAdapter = new RecyclerViewArrayListAdapter<Activities, ActivitiesListActivity.ActivitiesViewHolder>(listaActivities) {

            @NonNull
            @Override
            public ActivitiesListActivity.ActivitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.activities_list_item, parent, false);
                ActivitiesListActivity.ActivitiesViewHolder ss = new ActivitiesListActivity.ActivitiesViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull ActivitiesListActivity.ActivitiesViewHolder myHolder, int position) {


                final Activities myActivities = getData().get(position);

                myHolder.Completed.setText(myActivities.Completed);
                myHolder.dueDate.setText(myActivities.DueDate);
                myHolder.ActivitiesID.setText(myActivities.ActivitiesID);
                myHolder.ActivitiesTitle.setText(myActivities.ActivitiesTitle);





                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click pe" + myActivities.ActivitiesID,activity);

                        Intent myIntent = new Intent(activity, ActivitiesDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("Activitate transmisa", myActivities);
                        myIntent.putExtras(b);
                        ActivitiesListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(activitiesAdapter);

    }
}
