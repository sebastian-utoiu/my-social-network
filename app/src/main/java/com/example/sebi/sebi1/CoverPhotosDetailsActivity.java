package com.example.sebi.sebi1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;

public class CoverPhotosDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cover_photos_details);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        CoverPhotos coverPhotosTransmis = bundle.getParcelable("coverPhotos transmis");


        TextView CoverPhotosIDTextView = findViewById(R.id.CoverPhotosID_textView);
        TextView CoverPhotosIDBookTextView = findViewById(R.id.CoverPhotosIDBook_textView);
        TextView CoverPhotosUrlTextView = findViewById(R.id.CoverPhotosUrl_textView);


        CoverPhotosIDTextView.setText(coverPhotosTransmis.CoverPhotosID);
        CoverPhotosIDBookTextView.setText(coverPhotosTransmis.CoverPhotosIDBook);
        CoverPhotosUrlTextView.setText(coverPhotosTransmis.CoverPhotosUrl);



    }
}
