package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class CoverPhotosListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cover_photos_list_activity);

        recyclerView = findViewById(R.id.coverPhotos_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final CoverPhotos[] allCoverPhotos = ServiceConnectionManager.getInstance().getAllCoverPhotosFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           afiseazaCoverPhotos(allCoverPhotos);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    class  CoverPhotosViewHolder extends  RecyclerView.ViewHolder{

        TextView CoverPhotosID;
        TextView CoverPhotosIDBooks;
        TextView CoverPhotosUrl;




        public CoverPhotosViewHolder(View itemView) {
            super(itemView);
            CoverPhotosID= itemView.findViewById(R.id.CoverPhotos_ID);
            CoverPhotosIDBooks = itemView.findViewById(R.id.CoverPhotos_IDBooks);
            CoverPhotosUrl = itemView.findViewById(R.id.CoverPhotos_Url);


        }
    }

    public void afiseazaCoverPhotos(final CoverPhotos[] coverPhotos)
    {


        final ArrayList listaCoverPhotos= new ArrayList<>(Arrays.asList(coverPhotos));

        final Activity activity = this;



        RecyclerView.Adapter coverPhotosAdapter = new RecyclerViewArrayListAdapter<CoverPhotos, CoverPhotosListActivity.CoverPhotosViewHolder>(listaCoverPhotos) {

            @NonNull
            @Override
            public CoverPhotosListActivity.CoverPhotosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.cover_photos_list_item, parent, false);
                CoverPhotosListActivity.CoverPhotosViewHolder ss = new CoverPhotosListActivity.CoverPhotosViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull CoverPhotosListActivity.CoverPhotosViewHolder myHolder, int position) {


                final CoverPhotos myCoverPhotos = getData().get(position);

                myHolder.CoverPhotosID.setText(myCoverPhotos.CoverPhotosID);
                myHolder.CoverPhotosIDBooks.setText(myCoverPhotos.CoverPhotosIDBook);
                myHolder.CoverPhotosUrl.setText(myCoverPhotos.CoverPhotosUrl);





                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click" +myCoverPhotos.CoverPhotosID,activity);

                        Intent myIntent = new Intent(activity, CoverPhotosDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("coverPhotos transmis", myCoverPhotos);
                        myIntent.putExtras(b);
                        CoverPhotosListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(coverPhotosAdapter);

    }
}