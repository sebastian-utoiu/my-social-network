package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Book;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class BookListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_list_activity);


        recyclerView = findViewById(R.id.book_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Book[] allBooks = ServiceConnectionManager.getInstance().getAllBooksFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaToateCartile(allBooks);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();


    }

    class  BooksViewHolder extends  RecyclerView.ViewHolder{

        TextView BookDescription;
        TextView BookExcerpt;
        TextView BookID;
        TextView BookPageCount;
        TextView BookPublishDate;
        TextView BookTitle;




        public BooksViewHolder(View itemView) {
            super(itemView);
            BookDescription= itemView.findViewById(R.id.Book_Description);
            BookExcerpt = itemView.findViewById(R.id.Book_Excerpt);
            BookID = itemView.findViewById(R.id.Book_ID);
            BookPageCount = itemView.findViewById(R.id.Book_PageCount);
            BookPublishDate = itemView.findViewById(R.id.Book_PublishDate);
            BookTitle = itemView.findViewById(R.id.Book_Title);



        }
    }


    public void afiseazaToateCartile(final Book[] books)
    {


        final ArrayList listaDeCarti= new ArrayList<>(Arrays.asList(books));

        final Activity activity = this;



        RecyclerView.Adapter booksAdapter = new RecyclerViewArrayListAdapter<Book, BookListActivity.BooksViewHolder>(listaDeCarti) {

            @NonNull
            @Override
            public BookListActivity.BooksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.book_list_item, parent, false);
                BookListActivity.BooksViewHolder ss = new BookListActivity.BooksViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull BookListActivity.BooksViewHolder myHolder, int position) {


                final Book myBooks = getData().get(position);

                myHolder.BookDescription.setText(myBooks.BookDescription);
                myHolder.BookExcerpt.setText(myBooks.BookExcerpt);
                myHolder.BookID.setText(myBooks.BookID);
                myHolder.BookPageCount.setText(myBooks.BookPageCount);
                myHolder.BookPublishDate.setText(myBooks.BookPublishDate);
                myHolder.BookTitle.setText(myBooks.BookTitle);





                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click pe" + myBooks.BookID,activity);

                        Intent myIntent = new Intent(activity, BookDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("Carte transmisa", myBooks);
                        myIntent.putExtras(b);
                        BookListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(booksAdapter);

    }
}
