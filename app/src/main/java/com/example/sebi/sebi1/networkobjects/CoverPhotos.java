package com.example.sebi.sebi1.networkobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CoverPhotos implements Parcelable {

    @SerializedName(value = "CoverPhotosID", alternate = {"ID"})
      public String CoverPhotosID;

    @SerializedName(value = "CoverPhotosIDBook", alternate = {"IDBook"})
      public String CoverPhotosIDBook;

    @SerializedName(value = "CoverPhotosUrl", alternate = {"Url"})
      public String CoverPhotosUrl;

    protected CoverPhotos(Parcel in) {
        CoverPhotosID = in.readString();
        CoverPhotosIDBook = in.readString();
        CoverPhotosUrl = in.readString();
    }

    public static final Creator<CoverPhotos> CREATOR = new Creator<CoverPhotos>() {
        @Override
        public CoverPhotos createFromParcel(Parcel in) {
            return new CoverPhotos(in);
        }

        @Override
        public CoverPhotos[] newArray(int size) {
            return new CoverPhotos[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(CoverPhotosID);
        parcel.writeString(CoverPhotosIDBook);
        parcel.writeString(CoverPhotosUrl);
    }
}
