package com.example.sebi.sebi1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Book;

public class BookDetailsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_details_activity);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Book CarteTransmisa = bundle.getParcelable("Carte transmisa");


        TextView BookDescriptionTextView = findViewById(R.id.BookDescription_textview);
        TextView BookExcerptTextView = findViewById(R.id.BookExcerpt_textview);
        TextView BookIDTextView = findViewById(R.id.BookID_textview);
        TextView BookPageCountTextView = findViewById(R.id.BookPageCount_textview);
        TextView BookPublishDateTextView = findViewById(R.id.BookPublishDate_textview);
        TextView BookTitleTextView = findViewById(R.id.BookTitle_textview);


        BookDescriptionTextView.setText(CarteTransmisa.BookDescription);
        BookExcerptTextView.setText(CarteTransmisa.BookExcerpt);
        BookIDTextView.setText(CarteTransmisa.BookID);
        BookPageCountTextView.setText(CarteTransmisa.BookPageCount);
        BookPublishDateTextView.setText(CarteTransmisa.BookPublishDate);
        BookTitleTextView.setText(CarteTransmisa.BookTitle);
    }
}
