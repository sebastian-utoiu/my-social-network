package com.example.sebi.sebi1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Users;

public class UsersDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_details_activity);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Users utilizatorTransmis = bundle.getParcelable("Utilizator transmis");


        TextView UsersIDTextView = findViewById(R.id.UsersID_textview);
        TextView UsersPasswordTextView = findViewById(R.id.UsersPassword_textview);
        TextView UserNameTextView = findViewById(R.id.UserName_textview);


        UsersIDTextView.setText(utilizatorTransmis.UsersID);
        UsersPasswordTextView.setText(utilizatorTransmis.UsersPassword);
        UserNameTextView.setText(utilizatorTransmis.UserName);


    }
}
