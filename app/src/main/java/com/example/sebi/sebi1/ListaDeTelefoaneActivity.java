package com.example.sebi.sebi1;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.sebi.sebi1.obiectele.Huaweii;
import com.example.sebi.sebi1.obiectele.Iphone;
import com.example.sebi.sebi1.obiectele.Samsung;
import com.example.sebi.sebi1.obiectele.Telefon;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;

public class ListaDeTelefoaneActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_de_telefoane_activity);





        /// fac un array de telefoane

        final Samsung s = new Samsung();
        s.Culoare = Color.RED;
        final Iphone s2 = new Iphone();
        s2.Culoare = Color.GREEN;

        final Huaweii s3 = new Huaweii();
        s3.Culoare = Color.BLUE;

        final ArrayList<Telefon> listaDeTelefoane = new ArrayList<>();
        listaDeTelefoane.add(s);
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s3); //huawei
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s);
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s3); //huawei
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s);



        final Activity activity = this;

        recyclerView = findViewById(R.id.lista_de_telefoane);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        RecyclerView.Adapter adapter = new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(activity).inflate(R.layout.list_item_telefon, parent, false);
                ViewHolderTelefon vvvv = new ViewHolderTelefon(view);
                return  vvvv;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

                ViewHolderTelefon myHolder = (ViewHolderTelefon) holder;
                final Telefon telefon = listaDeTelefoane.get(position);
                myHolder.numeTelefon.setText(telefon.numeleTelefonului());
                myHolder.culoareTelefon.setBackgroundColor(telefon.Culoare);
                myHolder.tipTelefon.setText(telefon.tipulTelefonului());


                if (telefon instanceof Huaweii)
                {
                    Huaweii hh = (Huaweii)telefon;
                }

                myHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click pe telefon: "+ telefon.numeleTelefonului(), activity);
                    }
                });


            }

            @Override
            public int getItemCount() { return listaDeTelefoane.size(); }
        };


        recyclerView.setAdapter(adapter);

    }

    class  ViewHolderTelefon extends  RecyclerView.ViewHolder{

        TextView numeTelefon;
        TextView tipTelefon;
        FrameLayout culoareTelefon;


        public ViewHolderTelefon(View itemView) {
            super(itemView);
            numeTelefon = itemView.findViewById(R.id.nume_telefon_dat_de_noi);
            tipTelefon = itemView.findViewById(R.id.tip_telefon);
            culoareTelefon = itemView.findViewById(R.id.culoare_telefon);
        }
    }
}
