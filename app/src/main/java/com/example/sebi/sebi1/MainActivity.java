package com.example.sebi.sebi1;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.sebi.sebi1.fragments.MyFirstFragment;
import com.example.sebi.sebi1.networkactivities.TestGetPostPutActivity;
import com.example.sebi.sebi1.obiectele.Masina;
import com.example.sebi.sebi1.useful.Utils;

public class MainActivity extends AppCompatActivity
{

	LinearLayout myLayout;
	AnimationDrawable animationDrawable;
	public static final String DETAIL_TAG = "detailTa";

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_buttons_start );

		myLayout = (LinearLayout) findViewById( R.id.MyLayout );

		animationDrawable = (AnimationDrawable) myLayout.getBackground();
		animationDrawable.setEnterFadeDuration( 10 );
		animationDrawable.setExitFadeDuration( 5000 );
		animationDrawable.start();

		Button butonStart = findViewById( R.id.btnStart );
		butonStart.setText( "Noul start" );

		butonStart.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, Activity1.class );

				Masina m = new Masina( "dsa", "DSa", "Sda", "DSa", "0" );

				Bundle b = new Bundle();
				b.putParcelable( "masina transmisa", m );
				myIntent.putExtras( b );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button butonPause = findViewById( R.id.btnPause );
		butonPause.setText( "Navigate to first fragment" );

		butonPause.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
//				Intent myIntent = new Intent( MainActivity.this, TestGetPostPutActivity.class );
//
//				MainActivity.this.startActivity( myIntent );

				MyFirstFragment fragment = new MyFirstFragment();
				showDetail( fragment, false );

			}
		} );

		Button butonStop = findViewById( R.id.btnStop );

		butonStop.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, Activity3.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button butonBack = findViewById( R.id.btnBack );

		butonBack.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, Activity5.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button butonForward = findViewById( R.id.btnForward );

		butonForward.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, Activity6.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button butonHello = findViewById( R.id.btnHello );

		butonHello.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, Activity7.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button butonGo = findViewById( R.id.btnGo );

		butonGo.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, ListaDeTelefoaneActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaTelSIPers = findViewById( R.id.btnSalut );

		btnListaTelSIPers.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, ListaDeTelefoaneSiPersoaneActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaDeMasini = findViewById( R.id.btn_1 );

		btnListaDeMasini.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, ListaDeMasini.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaUseri = findViewById( R.id.navigate_to_user_list_button );

		btnListaUseri.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, UsersListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaPostari = findViewById( R.id.navigate_to_posts_list_button );

		btnListaPostari.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, PostsListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaAlbum = findViewById( R.id.navigate_to_album_list_button );

		btnListaAlbum.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, AlbumListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaTodos = findViewById( R.id.navigate_to_todos_list_button );

		btnListaTodos.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, TodosListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaAuthors = findViewById( R.id.navigate_to_authors_list_button );

		btnListaAuthors.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, AuthorsListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnCoverPhotos = findViewById( R.id.navigate_to_coverPhotos_list_button );

		btnCoverPhotos.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, CoverPhotosListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnUsers = findViewById( R.id.navigate_to_utilizatori_list_button );

		btnUsers.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, UsersListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnListaCarti = findViewById( R.id.navigate_to_books_list_button );

		btnListaCarti.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, BookListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnActivities = findViewById( R.id.navigate_to_activities_list_button );

		btnActivities.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{
				Intent myIntent = new Intent( MainActivity.this, ActivitiesListActivity.class );

				MainActivity.this.startActivity( myIntent );
			}
		} );

		Button btnMasiniSalvate = findViewById( R.id.btn_2 );

		btnMasiniSalvate.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{

				Masina m1 = Masina.citesteMasinaDinSharedPrefs( "1" );
				Masina m2 = Masina.citesteMasinaDinSharedPrefs( "2" );
				String masina1 = m1.numeMasina + m1.anFabricatie + m1.tip;
				String masina2 = m2.numeMasina + m2.anFabricatie + m2.tip;

				Utils.showMessageWithToast( masina1 + " \r\n " + masina2, App.contextulAplicatiei );
			}
		} );
	}


	//asta e metoda de folosit pentru navigare. Daca facem ca la activitati, parametrul e replace = false;
	public void showDetail( Fragment fragment, boolean replace )
	{
		if( fragment != null )
		{
			try
			{
				if( replace )
				{
					getSupportFragmentManager().popBackStackImmediate( DETAIL_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE );
				}

				///show fragment

				showFragment( fragment, DETAIL_TAG, true );
			}
			catch( Exception e )
			{
				Utils.logError( "Eroare show details for fragment", e );
			}
		}
	}

	private void showFragment( Fragment fragment, String tag, boolean addToBackStack )
	{
		try
		{
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace( R.id.fragments_container, fragment, tag );
			if( addToBackStack )
			{
				ft.addToBackStack( tag );
			}
			ft.commit();
		}
		catch( Exception e )
		{
			Utils.logError( "ddsadsa", e );
		}
	}
}

