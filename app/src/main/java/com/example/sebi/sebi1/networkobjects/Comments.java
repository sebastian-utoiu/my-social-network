package com.example.sebi.sebi1.networkobjects;

/*

{
    "postId": 1,
    "id": 1,
    "name": "id labore ex et quam laborum",
    "email": "Eliseo@gardner.biz",
    "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
  },
 */

public class Comments {

    public String postId;
    public String id;
    public String name;
    public String email;
    public String body;
}
