package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Album;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class AlbumListActivity extends AppCompatActivity {

   private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list_activity);


        recyclerView = findViewById(R.id.album_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));



        //Aici downloadez tot albumul

        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Album[] allAlbum = ServiceConnectionManager.getInstance().getAllAlbumFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazTotAlbumul(allAlbum);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();
    }



    class  AlbumViewHolder extends  RecyclerView.ViewHolder{


        TextView Id_album;
        TextView tip_id_album;
        TextView title_album;
        TextView url_album;
        TextView thumbnaiUrl_album;



        public AlbumViewHolder(View itemView) {
            super(itemView);
            Id_album= itemView.findViewById(R.id.Id_Album);
            tip_id_album = itemView.findViewById(R.id.tip_id_album);
            title_album= itemView.findViewById(R.id.title_album);
            url_album = itemView.findViewById(R.id.url_album);
            thumbnaiUrl_album = itemView.findViewById(R.id.thumbnaiUrl_album);

        }
    }

    public void afiseazTotAlbumul(final Album[] albums)
    {


        final ArrayList listaAlbum= new ArrayList<Album>(Arrays.asList(albums));

        final Activity activity = this;

        RecyclerView.Adapter albumAdapter = new RecyclerViewArrayListAdapter<Album, AlbumListActivity.AlbumViewHolder>(listaAlbum) {

            @NonNull
            @Override
            public AlbumListActivity.AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.album_list_item, parent, false);
                AlbumListActivity.AlbumViewHolder ss = new AlbumListActivity.AlbumViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull AlbumListActivity.AlbumViewHolder myHolder, int position) {


                final Album myAlbum = getData().get(position);
                myHolder.Id_album.setText(myAlbum.albumid);
                myHolder.tip_id_album.setText(myAlbum.albumTipId);
                myHolder.title_album.setText(myAlbum.albumTitle);
                myHolder.url_album.setText(myAlbum.url);
                myHolder.thumbnaiUrl_album.setText(myAlbum.thumbnailUrl);





                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("Click pe " +myAlbum.albumTitle,activity);

                        Intent myIntent = new Intent(activity, AlbumDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("album transmis", myAlbum);
                        myIntent.putExtras(b);
                        AlbumListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(albumAdapter);

    }
}

