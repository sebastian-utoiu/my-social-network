package com.example.sebi.sebi1.networkobjects;
/*
 {
    "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "https://via.placeholder.com/600/92c952",
    "thumbnailUrl": "https://via.placeholder.com/150/92c952"
  },
 */


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Album implements Parcelable {

    @SerializedName(value = "albumid", alternate = {"id"})
    public String albumid;

    public String albumTipId;

    @SerializedName(value = "albumTitle", alternate = {"title"})
    public String albumTitle;

    public String url;


    public String thumbnailUrl;


    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {


        destination.writeString(albumid);
        destination.writeString(albumTipId);
        destination.writeString(albumTitle);
        destination.writeString(url);
        destination.writeString(thumbnailUrl);
    }


    private Album(Parcel in) {
        albumid = in.readString();
        albumTipId = in.readString();
        albumTitle = in.readString();
        url = in.readString();
        thumbnailUrl =in.readString();


    }
}
