package com.example.sebi.sebi1.networkobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Book implements Parcelable {

    @SerializedName(value = "BookDescription", alternate = {"Description"})
    public String BookDescription;
    @SerializedName(value = "BookExcerpt", alternate = {"Excerpt"})
    public String BookExcerpt;
    @SerializedName(value = "BookID", alternate = {"ID"})
    public String BookID;
    @SerializedName(value = "BookPageCount", alternate = {"PageCount"})
    public String BookPageCount;
    @SerializedName(value = "BookPublishDate", alternate = {"PublishDate"})
    public String BookPublishDate;
    @SerializedName(value = "BookTitle", alternate = {"Title"})
    public String BookTitle;


    protected Book(Parcel in) {
        BookDescription = in.readString();
        BookExcerpt = in.readString();
        BookID = in.readString();
        BookPageCount = in.readString();
        BookPublishDate = in.readString();
        BookTitle = in.readString();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(BookDescription);
        parcel.writeString(BookExcerpt);
        parcel.writeString(BookID);
        parcel.writeString(BookPageCount);
        parcel.writeString(BookPublishDate);
        parcel.writeString(BookTitle);
    }
}
