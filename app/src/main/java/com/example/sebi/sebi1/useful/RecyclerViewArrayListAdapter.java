package com.example.sebi.sebi1.useful;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public abstract class RecyclerViewArrayListAdapter<T, ViewHolder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<ViewHolder>
{
    private ArrayList<T> mData;

    public RecyclerViewArrayListAdapter( ArrayList<T> data )
    {
        mData = data;
    }

    @Override
    public int getItemCount()
    {
        return mData == null ? 0 : mData.size();
    }

    public void setData( ArrayList<T> data )
    {
        mData = data;

        notifyDataSetChanged();
    }

    public ArrayList<T> getData()
    {
        return mData;
    }
}