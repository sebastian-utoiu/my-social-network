package com.example.sebi.sebi1;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.example.sebi.sebi1.obiectele.Persoana;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;

public class Activity3 extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);


        final Persoana p1 = new Persoana("Andrei", "23", "57");


        final Activity thisActivity = this;

        Button butonNume = findViewById(R.id.buton_nume);
        butonNume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(p1.nume, thisActivity);
            }
        });


        Button butonPrenume = findViewById(R.id.buton_prenume);
        butonPrenume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(p1.inaltime, thisActivity);
            }
        });


        Button butonVarsta = findViewById(R.id.buton_varsta);
        butonVarsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(p1.varsta, thisActivity);
            }
        });


        final ArrayList<String> nume=new ArrayList<>();

        nume.add("Ana");
        nume.add("Maria");
        nume.add("Marcel");
        nume.add("Marius");
        nume.add("Gabi");
        nume.add("Marina");
        nume.add("Inna");


        AppCompatSpinner spinner = findViewById(R.id.spinner_de_afisat_nume);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(thisActivity, R.layout.spinner_item, nume);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                String elementulSelectat = nume.get(position);
                Utils.showMessageWithToast("Elementul selectat :" +elementulSelectat, thisActivity);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Utils.showMessageWithToast("Nu am selectat nimic", thisActivity);
            }

        });





    }
}
