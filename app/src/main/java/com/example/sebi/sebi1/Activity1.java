package com.example.sebi.sebi1;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.example.sebi.sebi1.obiectele.Huaweii;
import com.example.sebi.sebi1.obiectele.Iphone;
import com.example.sebi.sebi1.obiectele.Samsung;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;

public class Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);


        //mai jos avem click pe butoane si cu afisarea unui mesaj (telefoane)  test
        final Samsung s = new Samsung();

        s.Culoare = Color.RED;

        final Iphone s2 = new Iphone();
        s2.Culoare= Color.BLUE;

        final Huaweii s3 = new Huaweii();

        final Huaweii s3222 = new Huaweii();

        final Activity thisActivity = this;

        Button butonSamsung = findViewById(R.id.buton_samsung);
        butonSamsung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(s.numeleTelefonului(), thisActivity);
            }
        });


        Button butonIphone = findViewById(R.id.buton_iphone);
        butonIphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(s2.numeleTelefonului(), thisActivity);
            }
        });

        Button butonHuaweii = findViewById(R.id.buton_huawei);
        butonHuaweii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showMessageWithToast(s3.numeleTelefonului(), thisActivity);
            }
        });

        //mai jos o sa facem o lista foarte simpla sa afisam diverse valori

        final ArrayList<String> valori = new ArrayList<>();
        valori.add("Ion");
        valori.add("Popa");
        valori.add("01");
        valori.add("02");
        valori.add("Iphone");


        AppCompatSpinner spinner = findViewById(R.id.spinner_de_afisat_valori);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(thisActivity, R.layout.spinner_item, valori);
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                String elementulSelectat = valori.get(position);
                Utils.showMessageWithToast("Elementul selectat :" +elementulSelectat, thisActivity);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                Utils.showMessageWithToast("Nu am selectat nimic", thisActivity);
            }

        });


        //de citit pt liste (recyclerview)
        //1. e mai simplu : https://stackoverflow.com/questions/40584424/simple-android-recyclerview-example
        //2. https://guides.codepath.com/android/using-the-recyclerview
        //3. https://www.codexpedia.com/android/a-very-simple-example-of-android-recyclerview/


    }
}
