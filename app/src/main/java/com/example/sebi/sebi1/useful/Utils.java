package com.example.sebi.sebi1.useful;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class Utils {
    public static void showMessageWithToast(String message, Context activity)
    {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static String readStringFromStream( InputStream is ) throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead = 0;
        StringBuilder sb = new StringBuilder();
        while( ( bytesRead = is.read( buffer ) ) != -1 )
        {
            sb.append( new String( buffer, 0, bytesRead ) );
        }

        return sb.toString();
    }

    public static void logError( String message, Exception e )
    {

        if( message != null )
        {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            String className = stackTrace[3].getClassName();
            String tag = className.substring( className.lastIndexOf( '.' ) + 1 );
            Log.e( tag, message );
        }

        if( e != null )
        {
            e.printStackTrace();
        }
    }
}
