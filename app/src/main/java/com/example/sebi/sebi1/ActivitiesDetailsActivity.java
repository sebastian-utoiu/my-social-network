package com.example.sebi.sebi1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Activities;
import com.example.sebi.sebi1.networkobjects.Users;

public class ActivitiesDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activities_details_activity);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Activities activitateTransmisa = bundle.getParcelable("Activitate transmisa");


        TextView CompletedTextView = findViewById(R.id.Completed_textview);
        TextView DueDateTextView = findViewById(R.id.DueDate_textview);
        TextView ActivitiesIDTextView = findViewById(R.id.ActivitiesID_textview);
        TextView ActivitiesTitleTextView = findViewById(R.id.ActivitiesTitle_textview);


        CompletedTextView.setText(activitateTransmisa.Completed);
        DueDateTextView.setText(activitateTransmisa.DueDate);
        ActivitiesIDTextView.setText(activitateTransmisa.ActivitiesID);
        ActivitiesTitleTextView.setText(activitateTransmisa.ActivitiesTitle);
    }
}
