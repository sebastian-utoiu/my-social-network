package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sebi.sebi1.obiectele.Masina;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ListaDeMasini extends AppCompatActivity {

    RecyclerView recyclerView;
    Adapter mAdapter;
    Intent myIntent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_de_masini_activity);

        // Array de Masini.

        Masina m1 = new Masina("Audi", "A2", "2007", "170","1");
        Masina m2 = new Masina("Audi", "A4", "2010", "300","2");
        Masina m3 = new Masina("Mercedes", "AMG", "2019", "500","3");
        Masina m4 = new Masina("Audi", "S8", "2008", "350","4");
        Masina m5 = new Masina("Audi", "RS5", "2017", "150","5");
        Masina m6 = new Masina("Renault", "Clio", "2018", "460","6");
        Masina m7 = new Masina("Mercedes", "CLS", "2019", "320","7");
        Masina m8 = new Masina("Volkswagen", "Passat", "2016", "170","8");
        Masina m9 = new Masina("Renault", "Kadjar", "2015", "150","9");
        Masina m10 = new Masina("BMW", "M4", "2012", "220","10");
        Masina m11 = new Masina("BMW", "M6", "2011", "190","11");


        m1.salveazaMasinaInSharedPrefs("1");
        m2.salveazaMasinaInSharedPrefs("2");




        final ArrayList<Masina> listaDeMasini = new ArrayList<>();
        listaDeMasini.add(m1);
        listaDeMasini.add(m2);
        listaDeMasini.add(m3);
        listaDeMasini.add(m4);
        listaDeMasini.add(m5);
        listaDeMasini.add(m6);
        listaDeMasini.add(m7);
        listaDeMasini.add(m8);
        listaDeMasini.add(m9);
        listaDeMasini.add(m10);
        listaDeMasini.add(m11);


        salveazaListaDeMasini(listaDeMasini, "lista_masini");


        ArrayList<Masina> listaSalvata =  citesteListaDeMasini("lista_masini");










        final Activity activity = this;

        recyclerView = findViewById(R.id.lista_de_masini);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        Adapter customAdapter = new RecyclerViewArrayListAdapter<Masina, ViewHolderMasina>(listaDeMasini) {

            @NonNull
            @Override
            public ViewHolderMasina onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(activity).inflate(R.layout.list_item_masini, parent, false);
                ViewHolderMasina ss = new ViewHolderMasina(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull ViewHolderMasina myHolder, int position) {


                final Masina masina=getData().get(position);
                myHolder.numeMasina.setText(masina.numeMasina());
                myHolder.tipulDeMasina.setText(masina.tip);
                myHolder.AnuldeFabricatie.setText(masina.anFabricatie());
                myHolder.PutereaMotorului.setText(masina.putereMotor);
                myHolder.salveazaMasina.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        masina.salveazaMasinaInSharedPrefs(masina.numeMasina);
                        masina.salveazaMasinaInSharedPrefs(masina.anFabricatie);
                        masina.salveazaMasinaInSharedPrefs(masina.putereMotor);
                        masina.salveazaMasinaInSharedPrefs(masina.tip);

                        Intent myIntent = new Intent(activity, DetaliuDeMasina.class);


                        Bundle b = new Bundle();
                        b.putParcelable("masina transmisa", masina);
                        myIntent.putExtras(b);
                        ListaDeMasini.this.startActivity(myIntent);

                    }
                });


                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                };
                myHolder.itemView.setOnClickListener(view);






                Utils.showMessageWithToast("S-a dat click pe masina" +masina.numeMasina(),activity);


            }
        };


        Adapter adapter = new Adapter() {


            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from(activity).inflate(R.layout.list_item_masini, parent, false);
                ViewHolderMasina ss = new ViewHolderMasina(view);
                return ss;

            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

                ViewHolderMasina myHolder = (ViewHolderMasina) holder;
               final Masina masina=listaDeMasini.get(position);
                myHolder.numeMasina.setText(masina.numeMasina());
                myHolder.tipulDeMasina.setText(masina.tip);
                myHolder.AnuldeFabricatie.setText(masina.anFabricatie());
                myHolder.PutereaMotorului.setText(masina.putereMotor);
                myHolder.salveazaMasina.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        masina.salveazaMasinaInSharedPrefs(masina.numeMasina);
                        masina.salveazaMasinaInSharedPrefs(masina.anFabricatie);
                        masina.salveazaMasinaInSharedPrefs(masina.putereMotor);
                        masina.salveazaMasinaInSharedPrefs(masina.tip);

                        Intent myIntent = new Intent(activity, DetaliuDeMasina.class);


                        Bundle b = new Bundle();
                        b.putParcelable("masina transmisa", masina);
                        myIntent.putExtras(b);
                        ListaDeMasini.this.startActivity(myIntent);

                    }
                });


                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                };
                myHolder.itemView.setOnClickListener(view);






                Utils.showMessageWithToast("S-a dat click pe masina" +masina.numeMasina(),activity);



            }

            @Override
            public int getItemCount() { return listaDeMasini.size(); }
        };


        recyclerView.setAdapter(adapter);


        //ca sa stergem lista de masini creata mai sus, si sa notificam si adapterul
        //listaDeMasini.clear();
        //adapter.notifyDataSetChanged();

    }





    class  ViewHolderMasina extends  RecyclerView.ViewHolder{

        TextView numeMasina;
        TextView tipulDeMasina;
        TextView AnuldeFabricatie;
        TextView PutereaMotorului;
        Button salveazaMasina;


        public ViewHolderMasina(View itemView) {
            super(itemView);
            numeMasina= itemView.findViewById(R.id.nume_masina);
            tipulDeMasina = itemView.findViewById(R.id.tipul_de_masina);
            AnuldeFabricatie = itemView.findViewById(R.id.an_fabricatie);
            PutereaMotorului= itemView.findViewById(R.id.putere_motor);
            salveazaMasina = itemView.findViewById(R.id.buton_salveaza_masina);
        }
    }


    public static ArrayList<Masina> citesteListaDeMasini(String idMasina)
    {
        String jsonCitit = App.getSharedPreferences().getString(idMasina, null);
        if (jsonCitit != null)
        {
            Type type = new TypeToken<ArrayList<Masina>>()
            {
            }.getType();


            ArrayList<Masina> masinaCitita = new Gson().fromJson(jsonCitit, type);
            return masinaCitita;
        }else
        {
            return null;
        }
    }


    void salveazaListaDeMasini(ArrayList<Masina> listMea, String idMasina)
    {
        String jsonss = new Gson().toJson(listMea);
        App.getSharedPreferences().edit().putString(idMasina, jsonss) .commit();
    }
}
