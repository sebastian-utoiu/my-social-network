package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.User;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class PostsListActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.posts_list_activity);

        recyclerView = findViewById(R.id.lista_de_postari);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        //Aici downloadez toate postarile

        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Posts[] allPosts = ServiceConnectionManager.getInstance().getAllPostsFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaToatePostarile(allPosts);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();


    }

    //@Override
    //public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    //
    //
    //    View viewTWO = LayoutInflater.from(parent.getContext()).inflate(R.layout.first_cardview, parent, false);
    //    ViewHolder rowTWO = new ViewHolder(viewTWO);


    class  PostViewHolder extends  RecyclerView.ViewHolder{

        TextView postUserID;
        TextView postId;
        TextView postTitle;
        TextView postBody;



        public PostViewHolder(View itemView) {
            super(itemView);
            postUserID= itemView.findViewById(R.id.postUserID);
            postId = itemView.findViewById(R.id.postID);
            postTitle= itemView.findViewById(R.id.postTitle);
            postBody = itemView.findViewById(R.id.postBody);

        }
    }

    public void afiseazaToatePostarile(final Posts[] posts)
    {


        final ArrayList listaDePostari= new ArrayList<>(Arrays.asList(posts));

        final Activity activity = this;



        RecyclerView.Adapter postsAdapter = new RecyclerViewArrayListAdapter<Posts, PostsListActivity.PostViewHolder>(listaDePostari) {

            @NonNull
            @Override
            public PostsListActivity.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.posts_list_item, parent, false);
                PostsListActivity.PostViewHolder ss = new PostsListActivity.PostViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull PostsListActivity.PostViewHolder myHolder, int position) {


                final Posts myPost = getData().get(position);

                myHolder.postUserID.setText(myPost.postUserId);
                myHolder.postId.setText(myPost.postid);
                myHolder.postTitle.setText(myPost.postTitle);
                myHolder.postBody.setText(myPost.postBody);




                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click pe postare" +myPost.postUserId,activity);

                        Intent myIntent = new Intent(activity, PostDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("postare transmisaa", myPost);
                        myIntent.putExtras(b);
                        PostsListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(postsAdapter);

    }

}
