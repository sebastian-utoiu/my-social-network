package com.example.sebi.sebi1.networkobjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Activities implements Parcelable {

    public String Completed;
    public String DueDate;

    @SerializedName(value = "ActivitiesID", alternate = {"ID"})
    public String ActivitiesID;

    @SerializedName(value = "ActivitiesTitle", alternate = {"Title"})
    public String ActivitiesTitle;


    protected Activities(Parcel in) {
        Completed = in.readString();
        DueDate = in.readString();
        ActivitiesID = in.readString();
        ActivitiesTitle = in.readString();
    }

    public static final Creator<Activities> CREATOR = new Creator<Activities>() {
        @Override
        public Activities createFromParcel(Parcel in) {
            return new Activities(in);
        }

        @Override
        public Activities[] newArray(int size) {
            return new Activities[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Completed);
        parcel.writeString(DueDate);
        parcel.writeString(ActivitiesID);
        parcel.writeString(ActivitiesTitle);
    }
}
