package com.example.sebi.sebi1;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.sebi.sebi1.obiectele.Huaweii;
import com.example.sebi.sebi1.obiectele.Iphone;
import com.example.sebi.sebi1.obiectele.Persoana;
import com.example.sebi.sebi1.obiectele.Samsung;
import com.example.sebi.sebi1.obiectele.Telefon;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;

public class ListaDeTelefoaneSiPersoaneActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_de_telefoane_si_persoane_activity);


        /// fac un array de telefoane si persoane

        final Samsung s = new Samsung();
        s.Culoare = Color.RED;
        final Iphone s2 = new Iphone();
        s2.Culoare = Color.GREEN;

        final Huaweii s3 = new Huaweii();
        s3.Culoare = Color.BLUE;

        Persoana p1 = new Persoana("Andrei", "23", "57");
        Persoana p2 = new Persoana("Ion", "53", "55");
        Persoana p3 = new Persoana("Horia", "a3", "inaltime medie");


        final ArrayList<Object> listaDeTelefoane = new ArrayList<>();
        listaDeTelefoane.add(s);
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(p3);
        listaDeTelefoane.add(s3); //huawei
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s);
        listaDeTelefoane.add(p1);
        listaDeTelefoane.add(p2);
        listaDeTelefoane.add(s3); //huawei
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(s);
        listaDeTelefoane.add(s2);
        listaDeTelefoane.add(p3);




        final Activity activity = this;

        recyclerView = findViewById(R.id.lista_de_telefoane_si_persoame);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        final int VIEW_TYPE_TELEFON = 0;
        final int VIEW_TYPE_PERSOANA = 1;


        RecyclerView.Adapter adapter = new RecyclerView.Adapter() {

            @Override
            public int getItemViewType(int position) {

                final Object obiect =  listaDeTelefoane.get(position);

                if (obiect instanceof Telefon)
                {
                    return VIEW_TYPE_TELEFON;
                }else
                {
                    return VIEW_TYPE_PERSOANA;
                }

            }

            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                if (viewType == VIEW_TYPE_TELEFON) {

                    View view = LayoutInflater.from(activity).inflate(R.layout.list_item_telefon, parent, false);
                    ViewHolderTelefon vvvv = new ViewHolderTelefon(view);
                    return vvvv;
                }else {
                    View view = LayoutInflater.from(activity).inflate(R.layout.list_item_persoana, parent, false);
                    ViewHolderPersoana vvvv = new ViewHolderPersoana(view);
                    return vvvv;
                }
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


                final Object obiect =  listaDeTelefoane.get(position);

                if (obiect instanceof Telefon)
                {
                    ViewHolderTelefon myHolder = (ViewHolderTelefon) holder;
                    final Telefon telefon = (Telefon) listaDeTelefoane.get(position);
                    myHolder.numeTelefon.setText(telefon.numeleTelefonului());
                    myHolder.culoareTelefon.setBackgroundColor(telefon.Culoare);
                    myHolder.tipTelefon.setText(telefon.tipulTelefonului());


                    if (telefon instanceof Huaweii)
                    {
                        Huaweii hh = (Huaweii)telefon;
                    }

                    myHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Utils.showMessageWithToast("S-a dat click pe telefon: "+ telefon.numeleTelefonului(), activity);
                        }
                    });

                }else
                {
                    ViewHolderPersoana myHolder = (ViewHolderPersoana) holder;
                    final Persoana persoana = (Persoana) listaDeTelefoane.get(position);
                    myHolder.numePersoana.setText(persoana.nume);
                    myHolder.prenumePersoana.setText(persoana.inaltime);
                    myHolder.varstaPersoamna.setText(persoana.varsta);


                    myHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Utils.showMessageWithToast("S-a dat click pe telefon: "+ persoana.nume, activity);
                        }
                    });

                }





            }

            @Override
            public int getItemCount() {
                return listaDeTelefoane.size();
            }
        };


        recyclerView.setAdapter(adapter);

    }

    class  ViewHolderTelefon extends  RecyclerView.ViewHolder{

        TextView numeTelefon;
        TextView tipTelefon;
        FrameLayout culoareTelefon;


        public ViewHolderTelefon(View itemView) {
            super(itemView);
            numeTelefon = itemView.findViewById(R.id.nume_telefon_dat_de_noi);
            tipTelefon = itemView.findViewById(R.id.tip_telefon);
            culoareTelefon = itemView.findViewById(R.id.culoare_telefon);
        }
    }


    class  ViewHolderPersoana extends  RecyclerView.ViewHolder{

        TextView numePersoana;
        TextView prenumePersoana;
        TextView varstaPersoamna;


        public ViewHolderPersoana(View itemView) {
            super(itemView);
            numePersoana = itemView.findViewById(R.id.nume_persoama);
            prenumePersoana = itemView.findViewById(R.id.prenume_persoana);
            varstaPersoamna = itemView.findViewById(R.id.varsta_persoana);
        }
    }
}
