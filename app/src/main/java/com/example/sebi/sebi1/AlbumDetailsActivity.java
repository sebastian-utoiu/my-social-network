package com.example.sebi.sebi1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Album;

public class AlbumDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_details_activity);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Album albumTransmis = bundle.getParcelable("album transmis");


        TextView Id_album = findViewById(R.id.albumId_textview);
        TextView tip_id_album = findViewById(R.id.albumTipId_textview);
        TextView title_album = findViewById(R.id.albumTitle_textview);
        TextView url_album = findViewById(R.id.url_textview);
        TextView thumbnaiUrl_album = findViewById(R.id.thumbnaiUrl_textview);

        Id_album.setText(albumTransmis.albumid);
        tip_id_album.setText(albumTransmis.albumTipId);
        title_album.setText(albumTransmis.albumTitle);
        url_album.setText(albumTransmis.url);
        thumbnaiUrl_album.setText(albumTransmis.thumbnailUrl);
    }
}
