package com.example.sebi.sebi1.networkobjects;

/*

  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  }
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.example.sebi.sebi1.obiectele.Masina;

public class User implements Parcelable {

    public String id;
    public String name;
    public String username;
    public String email;
    public Address address;
    public String phone;
    public String website;
    public Company company;


    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {

        destination.writeString(name);
        destination.writeString(username);
        destination.writeString(id);
        destination.writeString(email);
        destination.writeString(phone);
        destination.writeString(website);
        destination.writeParcelable(company,0);
    }


    protected User(Parcel in) {
        name = in.readString();
        username = in.readString();
        id = in.readString();
        email = in.readString();
        phone=in.readString();
        website=in.readString();
        company=in.readParcelable(Company.class.getClassLoader());

    }
}
