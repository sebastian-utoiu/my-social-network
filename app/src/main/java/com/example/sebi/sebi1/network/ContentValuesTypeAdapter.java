package com.example.sebi.sebi1.network;

import android.content.ContentValues;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Set;

//will be used after learning SQL
public class ContentValuesTypeAdapter extends TypeAdapter<ContentValues>
{
	private Class<? extends BaseColumns> mTableClass;

	public ContentValuesTypeAdapter( Class<? extends BaseColumns> tableClass )
	{
		mTableClass = tableClass;
	}

	@Override
	public ContentValues read( JsonReader in ) throws IOException
	{
		ArrayList<String> fieldNames = new ArrayList<String>();

		String tableName = getTableNameAndFields( mTableClass, fieldNames );

		String prefix = tableName + "_";
		ContentValues record = new ContentValues();
		in.beginObject();

		while( in.hasNext() )
		{
			String name = in.nextName();
			String columnName = prefix + name;

			Object value = null;

			switch( in.peek() )
			{
				case STRING:
				case NUMBER:
				case BOOLEAN:
					if( fieldNames.contains( columnName ) )
					{
						value = getValue( in, name );
					}
					else
					{
						in.skipValue();
					}
					break;

				case BEGIN_OBJECT:
					parseObject( in, name );
					break;

				case BEGIN_ARRAY:
					parseArray( in, name );
					break;

				default:
					in.skipValue();
					break;
			}

			if( value != null )
			{
				record.put( columnName, value.toString() );
			}
		}
		in.endObject();

		return record;
	}

	public static String getTableNameAndFields( Class<? extends BaseColumns> tableClass, ArrayList<String> fieldNames )
	{
		String tableName = null;
		Field[] fields = tableClass.getFields();
		for( Field field : fields )
		{
			Class<?> fieldClass = field.getDeclaringClass();
			if( fieldClass == tableClass )
			{
				String fieldValue = null;
				try
				{
					fieldValue = field.get( null ).toString();
				}
				catch( IllegalAccessException | IllegalArgumentException e )
				{
					e.printStackTrace( );
				}

				if( TextUtils.equals( field.getName(), "TABLE_NAME" ) )
				{
					tableName = fieldValue;
				}
				else
				{
					fieldNames.add( fieldValue );
				}
			}
		}
		return tableName;
	}

	@Override
	public void write( JsonWriter out, ContentValues value ) throws IOException
	{
		out.beginObject();
		Set<String> keySet = value.keySet();
		for( String key : keySet )
		{
			out.name( key ).value( value.getAsString( key ) );
		}
		out.endObject();
	}

	protected Object getValue( JsonReader in, String name ) throws IOException
	{
		if( in.peek() == JsonToken.BOOLEAN )
		{
			return in.nextBoolean();
		}
		return in.nextString();
	}

	protected void parseObject( JsonReader in, String name ) throws IOException
	{
		in.skipValue();
	}

	protected void parseArray( JsonReader in, String name ) throws IOException
	{
		in.skipValue();
	}
}
