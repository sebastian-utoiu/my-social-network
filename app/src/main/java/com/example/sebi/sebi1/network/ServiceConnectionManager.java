package com.example.sebi.sebi1.network;

import android.text.TextUtils;

import com.example.sebi.sebi1.networkobjects.Activities;
import com.example.sebi.sebi1.networkobjects.Album;
import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.Book;
import com.example.sebi.sebi1.networkobjects.Comments;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;
import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.Todos;
import com.example.sebi.sebi1.networkobjects.User;
import com.example.sebi.sebi1.networkobjects.Users;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServiceConnectionManager
{
	//singleton
	private static final class Holder
	{
		private static final ServiceConnectionManager instance = new ServiceConnectionManager();
	}


	public static ServiceConnectionManager getInstance()
	{
		return Holder.instance;
	}


	public static final MediaType JSON = MediaType.parse( "application/json; charset=utf-8" );


	protected OkHttpClient client;

	//constructor
	public ServiceConnectionManager()
	{
		OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
		builder.connectTimeout( 1, TimeUnit.MINUTES );
		builder.writeTimeout( 1, TimeUnit.MINUTES );
		builder.readTimeout( 1, TimeUnit.MINUTES );
		client = builder.build();
	}

	//----------------------metode de folosit----------------

	public <T> T get( String url, Parser<T> parser ) throws ConnectionException
	{
		return execute( url, "GET", null, parser );
	}

	public <T> T post( String url, String body, Parser<T> parser ) throws ConnectionException
	{
		return execute( url, "POST", body, parser );
	}

	public <T> T put( String url, String body, Parser<T> parser ) throws ConnectionException
	{
		return execute( url, "PUT", body, parser);
	}

	public <T> T delete( String url, Parser<T> parser ) throws ConnectionException
	{
		return execute( url, "DELETE", null, parser);
	}
	//----------------------------end metode de folosit--------------------

	//--------------------aici scriem metodele de get/post date de la server---------------------


    public User[] getAllUsersFromTheServer() throws Exception
    {
        String url = "https://jsonplaceholder.typicode.com/users";

        return get(url, new Parser<User[]>() {
            @Override
            protected User[] parse(InputStream is, long size) throws IOException {


                JsonReader reader = new JsonReader( new InputStreamReader( is ) );

				return new Gson().fromJson( reader, User[].class );
            }
        });
    }


    public Posts[] getAllPostsFromTheServer() throws Exception{

	    String url= "https://jsonplaceholder.typicode.com/posts";

	    return get(url, new Parser<Posts[]>() {
            @Override
            protected Posts[] parse(InputStream is, long size) throws IOException {

                JsonReader reader=new JsonReader(new InputStreamReader(is));

                return new Gson().fromJson(reader, Posts[].class);
            }
        });
    }


    public Comments[] getAllCommentsFromTheServer() throws Exception{

	    String url= "https://jsonplaceholder.typicode.com/comments";

	    return get(url, new Parser<Comments[]>() {
            @Override
            protected Comments[] parse(InputStream is, long size) throws IOException {

                JsonReader reader=new JsonReader( new InputStreamReader(is));

                return new Gson().fromJson(reader, Comments[].class);
            }
        });
    }


    public Author[] getAllAuthorsFromTheServer() throws Exception{

		String url= "https://fakerestapi.azurewebsites.net/api/Authors";

		return get(url, new Parser<Author[]>() {
			@Override
			protected Author[] parse(InputStream is, long size) throws IOException {


				JsonReader reader=new JsonReader(new InputStreamReader(is));

				return new Gson().fromJson(reader, Author[].class);
			}
		});
	}


	public Book[] getAllBooksFromTheServer() throws Exception{

		String url= "http://fakerestapi.azurewebsites.net/api/Books";

		return get(url, new Parser<Book[]>() {
			@Override
			protected Book[] parse(InputStream is, long size) throws IOException {

				JsonReader reader= new JsonReader(new InputStreamReader(is));

				return new Gson().fromJson(reader, Book[].class);
			}
		});
	}


	public Users[] getTotiUtilizatoriiFromTheServer() throws Exception{

		String url= "http://fakerestapi.azurewebsites.net/api/Users";

		return get(url, new Parser<Users[]>() {
			@Override
			protected Users[] parse(InputStream is, long size) throws IOException {

				JsonReader reader= new JsonReader(new InputStreamReader(is));

				return new Gson().fromJson(reader, Users[].class);
			}
		});
	}


	public CoverPhotos[] getAllCoverPhotosFromTheServer() throws Exception{

		String url = "http://fakerestapi.azurewebsites.net/api/CoverPhotos";

		return get(url, new Parser<CoverPhotos[]>() {
			@Override
			protected CoverPhotos[] parse(InputStream is, long size) throws IOException {

				JsonReader reader = new JsonReader(new InputStreamReader(is));

				return new Gson().fromJson(reader, CoverPhotos[].class);

			}
		});
	}


	public Activities[] getAllActivitiesFromTheServer() throws Exception{

		String url = "http://fakerestapi.azurewebsites.net/api/Activities";

		return get(url, new Parser<Activities[]>() {
			@Override
			protected Activities[] parse(InputStream is, long size) throws IOException {

				JsonReader reader = new JsonReader(new InputStreamReader(is));

				return new Gson().fromJson(reader, Activities[].class);

			}
		});
	}



    public Album[] getAllAlbumFromTheServer() throws Exception{

        String url= "https://jsonplaceholder.typicode.com/photos";

        return get(url, new Parser<Album[]>() {
            @Override
            protected Album[] parse(InputStream is, long size) throws IOException {

                JsonReader reader=new JsonReader( new InputStreamReader(is));

                return new Gson().fromJson(reader, Album[].class);
            }
        });
    }



    public Todos[] getAllTodosFromTheServer() throws Exception{

        String url="https://jsonplaceholder.typicode.com/todos";

        return get(url, new Parser<Todos[]>() {
            @Override
            protected Todos[] parse(InputStream is, long size) throws IOException {

                JsonReader reader=new JsonReader( new InputStreamReader(is));

                return new Gson().fromJson(reader, Todos[].class);
            }
        });
    }













//	public User[] getAllUsers() throws ConnectionException{
//		String url = "https://jsonplaceholder.typicode.com/users";
//
//		return  get( url, new Parser<User[]>()
//		{
//			@Override
//			protected User[] parse( InputStream is, long size ) throws IOException
//			{
//				JsonReader reader = new JsonReader( new InputStreamReader( is ) );
//
//				return new Gson().fromJson( reader, User[].class );
//			}
//		} );
//	}

	//--------------------pana aici scriem metodele de get/post date de la server---------------------

	// ////////////////////////////////////////////////////////////////
	//-------------Metoda generala care executa call-uri la server-------------------
	public <T> T execute( String url, String method, String body,  Parser<T> parser) throws ConnectionException
	{
		Request.Builder builder = new Request.Builder();
		try
		{
			builder.url( url );
		}
		catch( Exception e )
		{
			e.printStackTrace();
			return null;
		}

		RequestBody requestBody = body == null ? null : RequestBody.create( JSON, body );
		builder.method( method, requestBody );

		builder.addHeader( "Content-type", "application/json; charset=UTF-8" );
		builder.addHeader( "Accept-Encoding", "gzip, deflate" );

		Request request = builder.build();

		Response response = null;
		T result = null;

		try
		{
			response = client.newCall( request ).execute();


			if( parser != null )
			{
				String encoding = response.header( "Content-Encoding" );
				InputStream is = response.body().source().inputStream();
				if( TextUtils.equals( encoding, "gzip" ) || TextUtils.equals( encoding, "x-gzip" ) )
				{
					is = new GZIPInputStream( is );
				}

				if( response.isSuccessful() )
				{
					result = parser.parseSuccess( is, response.body().contentLength() );

				}
				else
				{
					Object error = parser.parseError( is, response.code() );

					throw new ConnectionException( response.code(), response.message(), error );
				}
			}
		}
		catch( UnknownHostException e )
		{
			throw new ConnectionException( 0, e.getMessage(), "No network connection!" );
		}
		catch( IOException e )
		{
			throw new ConnectionException( 0, e.getMessage(), "Network error!" );
		}
		finally
		{
			if( response != null )
			{
				response.close();
			}
		}

		return result;
	}

}
