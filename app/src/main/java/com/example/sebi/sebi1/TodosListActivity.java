package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.Todos;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class TodosListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todos_list_activity);



        recyclerView = findViewById(R.id.todos_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));



        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Todos[] allTodos = ServiceConnectionManager.getInstance().getAllTodosFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaTodos(allTodos);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();

    }



    class  TodosViewHolder extends  RecyclerView.ViewHolder{

        TextView todos_user_id;
        TextView todos_id;
        TextView todos_title;
        TextView todos_completed;



        public TodosViewHolder(View itemView) {
            super(itemView);
            todos_user_id= itemView.findViewById(R.id.todos_user_id);
            todos_id = itemView.findViewById(R.id.todos_id);
            todos_title= itemView.findViewById(R.id.todos_title);
            todos_completed = itemView.findViewById(R.id.todos_completed);

        }
    }

    public void afiseazaTodos(final Todos[] todos)
    {


        final ArrayList listaTodos= new ArrayList<>(Arrays.asList(todos));

        final Activity activity = this;



        RecyclerView.Adapter todosAdapter = new RecyclerViewArrayListAdapter<Todos, TodosListActivity.TodosViewHolder>(listaTodos) {

            @NonNull
            @Override
            public TodosListActivity.TodosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.todos_item, parent, false);
                TodosListActivity.TodosViewHolder ss = new TodosListActivity.TodosViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull TodosListActivity.TodosViewHolder myHolder, int position) {


                final Todos myTodo = getData().get(position);

                myHolder.todos_user_id.setText(myTodo.todosUserId);
                myHolder.todos_id.setText(myTodo.todosId);
                myHolder.todos_title.setText(myTodo.todosTitle);
                myHolder.todos_completed.setText(myTodo.completed);




                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click" +myTodo.todosUserId,activity);

                        Intent myIntent = new Intent(activity, TodosDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("todos transmis", myTodo);
                        myIntent.putExtras(b);
                        TodosListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(todosAdapter);

    }
}
