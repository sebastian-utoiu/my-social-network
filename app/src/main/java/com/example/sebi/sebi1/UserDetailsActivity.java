package com.example.sebi.sebi1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.User;
import com.example.sebi.sebi1.obiectele.Masina;

public class UserDetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        User userTransmis = bundle.getParcelable("user transmis");

        TextView nameTextView = findViewById(R.id.name_textview);
        TextView idTextView = findViewById(R.id.userId_textview);

        nameTextView.setText(userTransmis.name);



        idTextView.setText(userTransmis.id);



    }

}
