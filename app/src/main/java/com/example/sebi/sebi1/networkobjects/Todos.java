package com.example.sebi.sebi1.networkobjects;
/*
 {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  },
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Todos implements Parcelable {

    @SerializedName(value = "todosUserId", alternate = {"userId"})
    public String todosUserId;


    @SerializedName(value = "todosId", alternate = {"id"})
    public String todosId;


    @SerializedName(value = "todosTitle", alternate = {"title"})
    public String todosTitle;


    public String completed;


    public static final Creator<Todos> CREATOR = new Creator<Todos>() {
        @Override
        public Todos createFromParcel(Parcel in) {
            return new Todos(in);
        }

        @Override
        public Todos[] newArray(int size) {
            return new Todos[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {


        destination.writeString(todosUserId);
        destination.writeString(todosId);
        destination.writeString(todosTitle);
        destination.writeString(completed);

    }



    protected Todos(Parcel in) {
        todosUserId = in.readString();
        todosId = in.readString();
        todosTitle = in.readString();
        completed = in.readString();

    }
}
