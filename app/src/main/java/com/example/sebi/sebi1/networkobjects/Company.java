package com.example.sebi.sebi1.networkobjects;

/* "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
*/


import android.os.Parcel;
import android.os.Parcelable;

public class Company implements Parcelable {

    public String name;
    public String catchPhrase;
    public String bs;




    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {

        destination.writeString(name);
        destination.writeString(catchPhrase);
        destination.writeString(bs);

    }


    protected Company(Parcel in) {
        name = in.readString();
        catchPhrase = in.readString();
        bs = in.readString();

    }
}
