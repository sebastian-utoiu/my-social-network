package com.example.sebi.sebi1.obiectele;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.sebi.sebi1.App;
import com.google.gson.Gson;

public class Masina implements Parcelable {

    public String numeMasina;
    public String tip;
    public String anFabricatie;
    public String putereMotor;
    private String idMasina;


    public static final Creator<Masina> CREATOR = new Creator<Masina>() {
        @Override
        public Masina createFromParcel(Parcel in) {
            return new Masina(in);
        }

        @Override
        public Masina[] newArray(int size) {
            return new Masina[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {

        destination.writeString(numeMasina);
        destination.writeString(tip);
        destination.writeString(anFabricatie);
        destination.writeString(putereMotor);
        destination.writeString(idMasina);
    }

    public Masina(String numeMasina, String tip, String anFabricatie, String putereMotor, String idMasina)
    {
        super();
        this.numeMasina = numeMasina;
        this.tip =tip;
        this.anFabricatie=anFabricatie;
        this.putereMotor=putereMotor;
        this.idMasina=idMasina;

    }

    protected Masina(Parcel in) {
        numeMasina = in.readString();
        tip = in.readString();
        anFabricatie = in.readString();
        putereMotor = in.readString();
        idMasina=in.readString();
    }

    public String numeMasina() {
        return numeMasina;
    }

    public String anFabricatie() {
        return anFabricatie;
    }

    public String getIdMasina(){ return idMasina; }


    public void salveazaMasinaInSharedPrefs(String idMasina)
    {
        String jsonss = new Gson().toJson(this);
        App.getSharedPreferences().edit().putString(idMasina, jsonss) .commit();
    }

    public static Masina citesteMasinaDinSharedPrefs(String idMasina)
    {
        String jsonCitit = App.getSharedPreferences().getString(idMasina, null);
        ///App.getSharedPreferences().edit().remove("cheie"); //asa se face delete
        if (jsonCitit != null)
        {
            Masina masinaCitita = new Gson().fromJson(jsonCitit, Masina.class);
            return masinaCitita;
        }else
        {
            return null;
        }
    }


}
