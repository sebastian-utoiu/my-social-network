package com.example.sebi.sebi1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.Todos;

public class TodosDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todos_details_activity);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        Todos todosTransmis = bundle.getParcelable("todos transmis");


        TextView todos_user_id_TextView = findViewById(R.id.todos_user_id_textview);
        TextView todos_id_textview = findViewById(R.id.todos_id_textview);
        TextView todos_title_textview = findViewById(R.id.todos_title_textview);
        TextView completed_textview = findViewById(R.id.completed_textview);

        todos_user_id_TextView.setText(todosTransmis.todosUserId);
        todos_id_textview.setText(todosTransmis.todosId);
        todos_title_textview.setText(todosTransmis.todosTitle);
        completed_textview.setText(todosTransmis.completed);
    }
}
