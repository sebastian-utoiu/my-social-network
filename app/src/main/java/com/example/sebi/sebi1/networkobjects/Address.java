package com.example.sebi.sebi1.networkobjects;
/*
{
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
 */

public class Address {

    public String street;
    public String suite;
    public String city;
    public String zipCode;
    public GeoLocation geo;
}
