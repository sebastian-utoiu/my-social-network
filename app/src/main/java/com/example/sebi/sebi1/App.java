package com.example.sebi.sebi1;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class App extends Application {

    public App()
    {
        contextulAplicatiei = this;
    }

    public static App contextulAplicatiei;


    public static SharedPreferences getSharedPreferences()
    {
        return PreferenceManager.getDefaultSharedPreferences( contextulAplicatiei );
    }
}
