package com.example.sebi.sebi1.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sebi.sebi1.MainActivity;
import com.example.sebi.sebi1.R;
import com.example.sebi.sebi1.useful.Utils;

public class MyFirstFragment extends Fragment
{

	@Override
	public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.my_first_fragment, container, false );
		return view;
	}

	@Override
	public void onActivityCreated( @Nullable Bundle savedInstanceState )
	{
		super.onActivityCreated( savedInstanceState );


		Button b = getView().findViewById( R.id.my_first_fragment_button );
		b.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				MySecondFragment fragment = new MySecondFragment();
				((MainActivity)getActivity()).showDetail( fragment, false );

				Utils.showMessageWithToast( "Navigated to second fragment ", getActivity() );
			}
		} );



	}
}
