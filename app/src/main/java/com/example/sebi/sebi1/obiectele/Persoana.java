package com.example.sebi.sebi1.obiectele;

public class Persoana {

    public String nume;
    public String varsta;
    public String inaltime;

    public Persoana(String nume, String varsta, String inaltime)
    {
        super();
        this.nume = nume;
        this.varsta =varsta;
        this.inaltime = inaltime;
    }

}
