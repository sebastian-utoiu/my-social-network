package com.example.sebi.sebi1.network;

import android.content.ContentValues;
import android.provider.BaseColumns;

import com.example.sebi.sebi1.useful.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;


import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public abstract class Parser<T>
{

	//to be implemented for now
	protected abstract T parse( InputStream is, long size ) throws IOException;

	public T parseSuccess( InputStream is, long size )
	{
		try
		{
			return parse( is, size );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}
		return null;
	}

	public Object parseError( InputStream is, int responseCode ) throws IOException
	{
		String text = null;
		try
		{
			text = Utils.readStringFromStream( is );
			return new JSONObject( text ).getString( "error" );
		}
		catch( Exception e )
		{
			Utils.logError( "Error while parsing error stream: " + text, e );
		}
		return null;
	}

	//will be used after learning SQL
	protected static ContentValues getRecord( InputStream is, Class<? extends BaseColumns> tableClass )
	{
		JsonReader reader = new JsonReader( new InputStreamReader( is ) );
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter( ContentValues.class, new ContentValuesTypeAdapter( tableClass ) );
		Gson gson = gsonBuilder.create();
		ContentValues record = gson.fromJson( reader, ContentValues.class );

		return record;
	}

	//will be used after learning SQL
	protected static ArrayList<ContentValues> getRecords( InputStream is, Class<? extends BaseColumns> tableClass ) throws IOException
	{
		JsonReader reader = new JsonReader( new InputStreamReader( is ) );
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter( ContentValues.class, new ContentValuesTypeAdapter( tableClass ) );
		Gson gson = gsonBuilder.create();

		ArrayList<ContentValues> records = new ArrayList<ContentValues>();
		reader.beginArray();
		while( reader.peek() == JsonToken.BEGIN_OBJECT )
		{
			ContentValues record = gson.fromJson( reader, ContentValues.class );
			records.add( record );
		}
		reader.endArray();

		return records;
	}

	//to be used when we don't care about parsing the result returned by the server

	public static final class DummyParser extends Parser<Void>
	{
		@Override
		protected Void parse( InputStream is, long size ) throws IOException
		{
			return null;
		}
	}

	String nextValue( JsonReader in ) throws IOException
	{
		if( in.peek() != JsonToken.NULL )
		{
			return in.nextString();
		}
		else
		{
			in.skipValue();
			return null;
		}
	}
}
