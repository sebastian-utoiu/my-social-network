package com.example.sebi.sebi1.networkactivities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sebi.sebi1.R;
import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Activities;
import com.example.sebi.sebi1.networkobjects.Album;
import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.Book;
import com.example.sebi.sebi1.networkobjects.Comments;
import com.example.sebi.sebi1.networkobjects.CoverPhotos;
import com.example.sebi.sebi1.networkobjects.Posts;
import com.example.sebi.sebi1.networkobjects.Todos;
import com.example.sebi.sebi1.networkobjects.User;
import com.example.sebi.sebi1.networkobjects.Users;

public class TestGetPostPutActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_get_post_put_activity);


        Button butonGetAllUsers = findViewById(R.id.buton_get_all_users);

        butonGetAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               // aici rulam pe thread de background, adica desface un nou thread
                    new Thread(new Runnable() {
                        @Override
                        public void run() {


                            try {
                                final User[] allUsers = ServiceConnectionManager.getInstance().getAllUsersFromTheServer();

                                //aici ne intoarcem pe threadul principal, adica threadul de UI
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        afiseazaTotiUsersii(allUsers);
                                    }
                                });



                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }


                        }
                    }).start();


                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                final Posts[] allPosts = ServiceConnectionManager.getInstance().getAllPostsFromTheServer();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        afiseazaToatePostarile(allPosts);
                                    }
                                });


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();


                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            final Comments[] allComments = ServiceConnectionManager.getInstance().getAllCommentsFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaToateCommurile(allComments);
                                }
                            });



                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }
                }).start();


                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            final Album[] allAlbums = ServiceConnectionManager.getInstance().getAllAlbumFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaTotAlbumul(allAlbums);
                                }
                            });



                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }
                }).start();

                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            final Todos[] allTodos = ServiceConnectionManager.getInstance().getAllTodosFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaTodos(allTodos);
                                }
                            });



                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }
                }).start();


                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            final Author[] allAuthors = ServiceConnectionManager.getInstance().getAllAuthorsFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaTotiAutorii(allAuthors);
                                }
                            });



                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }
                }).start();



                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try{
                            final Book[] allBooks = ServiceConnectionManager.getInstance().getAllBooksFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaToateCartile(allBooks);
                                }
                            });
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }).start();



                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try{
                            final Users[] totiUtilizatorii = ServiceConnectionManager.getInstance().getTotiUtilizatoriiFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaUtilizatori(totiUtilizatorii);
                                }
                            });
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }).start();



                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try{
                            final  Activities[] allActivities = ServiceConnectionManager.getInstance().getAllActivitiesFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaToateActivitatile(allActivities);
                                }
                            });
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }).start();




                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            final CoverPhotos[] allCoverPhotos = ServiceConnectionManager.getInstance().getAllCoverPhotosFromTheServer();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    afiseazaCoverPhotos(allCoverPhotos);
                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }).start();



            }
        });

    }


    public void afiseazaTotiUsersii(User[] users)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(users.toString());

    }


    public void afiseazaToateActivitatile(Activities[] activities)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(activities.toString());

    }


    public void afiseazaUtilizatori(Users[] users)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(users.toString());

    }


    public void afiseazaToateCartile(Book[] books)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(books.toString());
    }

    public void afiseazaTotiAutorii(Author[] authors)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(authors.toString());

    }

    public void afiseazaCoverPhotos(CoverPhotos[] coverPhotos)
    {
        TextView textView = findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(coverPhotos.toString());

    }

    public void afiseazaToatePostarile(Posts[] posts){
        TextView textView=findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(posts.toString());
    }

    public void afiseazaToateCommurile(Comments[] comments){

        TextView textView=findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(comments.toString());
    }

    public void afiseazaTotAlbumul(Album[] albums){

        TextView textView=findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(albums.toString());
    }

    public void afiseazaTodos(Todos[] todos){

        TextView textView=findViewById(R.id.textview_ce_call_am_facut);
        textView.setText(todos.toString());
    }


}
