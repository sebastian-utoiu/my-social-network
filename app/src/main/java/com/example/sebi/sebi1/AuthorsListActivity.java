package com.example.sebi.sebi1;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sebi.sebi1.network.ServiceConnectionManager;
import com.example.sebi.sebi1.networkobjects.Author;
import com.example.sebi.sebi1.networkobjects.Todos;
import com.example.sebi.sebi1.useful.RecyclerViewArrayListAdapter;
import com.example.sebi.sebi1.useful.Utils;

import java.util.ArrayList;
import java.util.Arrays;

public class AuthorsListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authors_list_activity);

        recyclerView = findViewById(R.id.authors_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));



        new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    final Author[] allAuthors = ServiceConnectionManager.getInstance().getAllAuthorsFromTheServer();

                    //aici ne intoarcem pe threadul principal, adica threadul de UI
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            afiseazaTotiAutorii(allAuthors);
                        }
                    });



                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    class  AuthorsViewHolder extends  RecyclerView.ViewHolder{

        TextView FirstName;
        TextView ID;
        TextView IDBook;
        TextView LastName;



        public AuthorsViewHolder(View itemView) {
            super(itemView);
            FirstName= itemView.findViewById(R.id.FirstName_authors);
            ID = itemView.findViewById(R.id.ID_authors);
            IDBook= itemView.findViewById(R.id.IDBook_authors);
            LastName = itemView.findViewById(R.id.LastName_authors);

        }
    }

    public void afiseazaTotiAutorii(final Author[] authors)
    {


        final ArrayList listAuthors= new ArrayList<>(Arrays.asList(authors));

        final Activity activity = this;



        RecyclerView.Adapter authorsAdapter = new RecyclerViewArrayListAdapter<Author, AuthorsListActivity.AuthorsViewHolder>(listAuthors) {

            @NonNull
            @Override
            public AuthorsListActivity.AuthorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = LayoutInflater.from( activity).inflate(R.layout.authors_list_item, parent, false);
                AuthorsListActivity.AuthorsViewHolder ss = new AuthorsListActivity.AuthorsViewHolder(view);
                return ss;
            }

            @Override
            public void onBindViewHolder(@NonNull AuthorsListActivity.AuthorsViewHolder myHolder, int position) {


                final Author myAuthor = getData().get(position);

                myHolder.FirstName.setText(myAuthor.FirstName);
                myHolder.ID.setText(myAuthor.ID);
                myHolder.IDBook.setText(myAuthor.IDBook);
                myHolder.LastName.setText(myAuthor.LastName);




                View.OnClickListener view = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.showMessageWithToast("S-a dat click" +myAuthor.IDBook,activity);

                        Intent myIntent = new Intent(activity, AuthorsDetailsActivity.class);


                        Bundle b = new Bundle();
                        b.putParcelable("authors transmis", myAuthor);
                        myIntent.putExtras(b);
                        AuthorsListActivity.this.startActivity(myIntent);

                    }
                };
                myHolder.itemView.setOnClickListener(view);









            }
        };

        recyclerView.setAdapter(authorsAdapter);

    }
}
